Demo Blog application
=====================

This is demo blog application.

To install all dependencies run the following command:

    composer.phar --dev install

You might want to install composer first: http://getcomposer.org/download/

## Running the tests

To run all tests and verifications just type:

    ant

You can run certain tests selectively, like the Behat feature tests:

    ant test-feature

or the unit tests (with PHPUnit):

    ant test-unit

## The bundles

There are two bundles used in this demo application:

* DemoBlogBundle
* DemoApplicationBundle

The bundles are extending from the FOSUserBundle to add user management
(registration, login) to the application. The ApplicationBundle provides the
basic layout for the application, which bases on Twitters Bootstrap. The
BlogBundle implements the actual (simple) blog.

## Commands

This sections lists a number of command commands

### Bundle generation

To create a new bundle execute:

    app/console generate:bundle --namespace=Demo/BlogBundle

### Doctrine

After configuring your schema you run the following to update the scema in the
database:

    app/console doctrine:database:create
    app/console doctrine:schema:update --force

After creating or updating your entities you can run the following command to
create / update the corresponding entity class:

    app/console doctrine:generate:entities Demo

or more specifically:

    app/console doctrine:generate:entities Demo/BlogBundle/Entity/Post

**If** you want to create CRUD controller for your entities, you can run:

    app/console generate:doctrine:crud

### Assetic

If you want to install the assets to the web folder you can run:

    app/console assets:install
