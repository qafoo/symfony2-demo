<?php

namespace Demo\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;

use Demo\BlogBundle\Entity\Comment;
use Demo\BlogBundle\Entity\Post;
use Demo\BlogBundle\Form\CommentType;

/**
 * Comment controller.
 *
 */
class CommentController extends Controller
{
    /**
     * Creates a new Comment entity.
     *
     */
    public function createAction(Request $request, Post $post)
    {
        $entity  = new Comment();
        $entity->setPost($post);

        $form = $this->createForm(new CommentType(), $entity);
        $form->bind($request);

        $valid = true;
        $spamChecker = $this->container->get('DemoBlogBundle.Service.SpamChecker');
        if ($spamChecker->isSpam($entity)) {
            $valid = false;
            $this->get('session')->getFlashBag()->add(
                'error',
                'Your comment was considered spam.'
            );
        }

        if ($valid && !$form->isValid()) {
            $valid = false;
        }

        if ($valid) {
            $this->get('session')->getFlashBag()->add(
                'success',
                'Your comment has made it into the blog.'
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            $this->get('DemoBlogBundle.Service.SignalSlot')
                ->emit('commentCreated', array($entity));

        }

        return $this->redirect($this->generateUrl('DemoBlogBundle.Post.show', array('id' => $post->getId())));
    }

    /**
     * Displays a form to create a new Comment entity.
     *
     */
    public function newAction(Post $post)
    {
        $entity = new Comment();
        $entity->setPost($post);
        $form   = $this->createForm(new CommentType(), $entity);

        return $this->render(
            'DemoBlogBundle:Comment:new.html.twig',
            array(
                'post'   => $this->getRequest()->get('post'),
                'entity' => $entity,
                'form'   => $form->createView(),
            )
        );
    }

    /**
     * Deletes a Comment entity.
     *
     */
    public function deleteAction(Request $request, Comment $comment, Post $post)
    {
        $form = $this->createDeleteForm($comment->getId());
        $form->bind($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entity = $entityManager->getRepository('DemoBlogBundle:Comment')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Comment entity.');
            }

            $entityManager->remove($entity);
            $entityManager->flush();

            $this->get('session')->getFlashBag()->add('success', 'The comment has been removed.');
        }

        return $this->redirect($this->generateUrl('DemoBlogBundle.Post.show', array('id' => $post->getId())));
    }

    /**
     * Creates a form to delete a Comment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }
}
