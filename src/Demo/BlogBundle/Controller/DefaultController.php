<?php

namespace Demo\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render(
            // BlogBundle/Resources/views/Default/index.html.twig
            'DemoBlogBundle:Default:index.html.twig',
            array(
            )
        );
    }
}
