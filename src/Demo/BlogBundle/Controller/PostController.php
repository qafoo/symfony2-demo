<?php

namespace Demo\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Demo\BlogBundle\Entity\Post;
use Demo\BlogBundle\Form\PostType;

/**
 * Post controller.
 *
 */
class PostController extends Controller
{
    /**
     * Lists all Post entities.
     *
     */
    public function indexAction()
    {
        $postRepository = $this->container->get('DemoBlogBundle.Entity.PostRepository');

        $posts = $postRepository->getPostsWithComments();

        return $this->render(
            'DemoBlogBundle:Post:index.html.twig',
            array(
                'posts' => $posts,
            )
        );
    }

    /**
     * Creates a new Post entity.
     *
     */
    public function createAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(new PostType(), $post);
        $post->setUser(
            $this->get('security.context')->getToken()->getUser()
        );
        $form->bind($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('DemoBlogBundle.Post.show', array('id' => $post->getId())));
        }

        return $this->render(
            'DemoBlogBundle:Post:new.html.twig',
            array(
                'post' => $post,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Displays a form to create a new Post entity.
     *
     */
    public function newAction()
    {
        $post = new Post();
        $form = $this->createForm(new PostType(), $post);

        return $this->render(
            'DemoBlogBundle:Post:new.html.twig',
            array(
                'post' => $post,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a Post entity.
     *
     */
    public function showAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository('DemoBlogBundle:Post')->find($id);

        if (!$post) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $paginator = $this->get('knp_paginator');

        return $this->render(
            'DemoBlogBundle:Post:show.html.twig',
            array(
                'post' => $post,
                'comments' => $paginator->paginate(
                    $post->getComments(),
                    $request->get('page', 1),
                    5
                ),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     */
    public function editAction(Post $post)
    {
        $editForm = $this->createForm(new PostType(), $post);
        $deleteForm = $this->createDeleteForm($post->getId());

        return $this->render(
            'DemoBlogBundle:Post:edit.html.twig',
            array(
                'post' => $post,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Edits an existing Post entity.
     *
     */
    public function updateAction(Request $request, Post $post)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($post->getId());
        $editForm = $this->createForm(new PostType(), $post);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('DemoBlogBundle.Post.edit', array('post' => $post->getId())));
        }

        return $this->render(
            'DemoBlogBundle:Post:edit.html.twig',
            array(
                'post' => $post,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a Post entity.
     *
     */
    public function deleteAction(Request $request, $post)
    {
        $form = $this->createDeleteForm($post);
        $form->bind($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $post = $entityManager->getRepository('DemoBlogBundle:Post')->find($post);

            if (!$post) {
                throw $this->createNotFoundException('Unable to find Post entity.');
            }

            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirect($this->generateUrl('DemoBlogBundle.Post.index'));
    }

    /**
     * Creates a form to delete a Post entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }
}
