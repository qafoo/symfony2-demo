<?php


namespace Demo\BlogBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User entity
 */
class User extends BaseUser
{
    /**
     * ID
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }
}
