<?php

namespace Demo\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    /**
     * Get posts with comment count
     *
     * @return Demo\BlogBundle\Entity\Post[]
     */
    public function getPostsWithComments()
    {
        $dql = "SELECT
                p, c
            FROM
                Demo\BlogBundle\Entity\Post p
            LEFT JOIN
                p.comments c";

        $query = $this->getEntityManager()->createQuery($dql);

        return $query->getResult();
    }
}
