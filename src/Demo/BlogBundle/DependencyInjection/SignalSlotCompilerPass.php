<?php

namespace Demo\BlogBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class SignalSlotCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('DemoBlogBundle.Service.SignalSlot')) {
            return;
        }

        $definition = $container->getDefinition(
            'DemoBlogBundle.Service.SignalSlot'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'signal.listener'
        );

        foreach ($taggedServices as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                $definition->addMethodCall(
                    'register',
                    array($attributes['signal'], new Reference($id))
                );
            }
        }
    }
}
