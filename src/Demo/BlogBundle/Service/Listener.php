<?php

namespace Demo\BlogBundle\Service;

use Demo\BlogBundle\Entity\Comment;
use Symfony\Bridge\Monolog\Logger;

class Listener
{
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function commentCreated(Comment $comment)
    {
        $this->logger->notice("Comment created by " . $comment->getAuthor());
    }
}
