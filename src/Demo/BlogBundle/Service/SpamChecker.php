<?php

namespace Demo\BlogBundle\Service;

use Demo\BlogBundle\Entity\Comment;

/**
 * Comment spam checker
 */
abstract class SpamChecker
{
    /**
     * Is comment spam?
     *
     * @param Comment $comment
     * @return boolean
     */
    abstract public function isSpam(Comment $comment);
}
