<?php

namespace Demo\BlogBundle\Service;

class SignalSlot
{
    protected $slots = array();

    public function register($slot, $target)
    {
        $this->slots[$slot][] = $target;
    }

    public function emit($signal, array $parameters = array())
    {
        if (!isset($this->slots[$signal])) {
            return;
        }

        foreach ($this->slots[$signal] as $target) {
            call_user_func_array(
                array($target, $signal),
                $parameters
            );
        }
    }
}
