<?php

namespace Demo\BlogBundle\Service\SpamChecker;

use Demo\BlogBundle\Service\SpamChecker;
use Demo\BlogBundle\Entity\Comment;

/**
 * Comment spam checker
 */
class Akismet extends SpamChecker
{
    /**
     * API key for Akisment
     *
     * @var mixed
     */
    protected $apiKey;

    /**
     * Base URL of website / blog
     *
     * @var string
     */
    protected $baseUrl;

    /**
     * HTTP client to issue requests against Akismet
     *
     * @var HttpClient
     */
    protected $client;

    const BASE_URL = '.rest.akismet.com/1.1/comment-check';

    public function __construct(Akismet\HttpClient $client, $baseUrl, $apiKey)
    {
        $this->client = $client;
        $this->baseUrl = $baseUrl;
        $this->apiKey = $apiKey;
    }

    /**
     * Is comment spam?
     *
     * @param Comment $comment
     * @return boolean
     */
    public function isSpam(Comment $comment)
    {
        $response = $this->client->request(
            'POST',
            'http://' . $this->apiKey . self::BASE_URL,
            new Akismet\Message(
                array(),
                http_build_query(
                    array(
                        'blog'            => $this->baseUrl,
                        'user_ip'         => $_SERVER['REMOTE_ADDR'],
                        'user_agent'      => 'DemoBlog',
                        'referrer'        => ( isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '' ),
                        'comment_type'    => 'comment',
                        'comment_content' => $comment->getText(),
                        'comment_author'  => $comment->getAuthor(),
                    )
                )
            )
        );

        switch ($response->body) {
            case 'false':
                return false;
            case 'true':
                return true;
            default:
                throw new \RuntimeException("Reponse by Akismet: " . $response->body);
        }
    }
}
