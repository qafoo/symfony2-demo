<?php

namespace Demo\BlogBundle\Service\SpamChecker\Akismet;

/**
 * Interface for Http Client implementations
 */
abstract class HttpClient
{
    /**
     * Execute a HTTP request to the remote server
     *
     * Returns the result from the remote server.
     *
     * @param string $method
     * @param string $path
     * @param \eZ\Publish\Core\Persistence\Solr\Content\Search\Gateway\Message $message
     *
     * @return \eZ\Publish\Core\Persistence\Solr\Content\Search\Gateway\Message
     */
    abstract public function request($method, $path, Message $message = null);
}
