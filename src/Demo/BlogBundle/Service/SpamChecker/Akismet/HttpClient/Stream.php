<?php

namespace Demo\BlogBundle\Service\SpamChecker\Akismet\HttpClient;

use Demo\BlogBundle\Service\SpamChecker\Akismet\HttpClient;
use Demo\BlogBundle\Service\SpamChecker\Akismet\Message;

/**
 * Simple PHP stream based HTTP client.
 */
class Stream extends HttpClient
{
    /**
     * Execute a HTTP request to the remote server
     *
     * Returns the result from the remote server.
     *
     * @param string $method
     * @param string $url
     * @param Message $message
     * @return Message
     */
    public function request($method, $url, Message $message = null)
    {
        $message = $message ?: new Message();
        $requestHeaders = $this->getRequestHeaders($message->headers);

        $contextOptions = array(
            'http' => array(
                'method'        => $method,
                'content'       => $message->body,
                'ignore_errors' => true,
                'header'        => $requestHeaders,
            ),
        );

        $httpFilePointer = @fopen($url, 'r', false, stream_context_create($contextOptions));

        // Check if connection has been established successfully
        if ($httpFilePointer === false) {
            throw new ConnectionException($url, $method);
        }

        // Read request body
        $body = '';
        while (!feof($httpFilePointer)) {
            $body .= fgets($httpFilePointer);
        }

        $metaData   = stream_get_meta_data($httpFilePointer);
        // This depends on PHP compiled with or without --curl-enable-streamwrappers
        $rawHeaders = isset($metaData['wrapper_data']['headers']) ?
            $metaData['wrapper_data']['headers'] :
            $metaData['wrapper_data'];
        $headers = array();

        foreach ($rawHeaders as $lineContent) {
            // Extract header values
            if (preg_match('(^HTTP/(?P<version>\d+\.\d+)\s+(?P<status>\d+))S', $lineContent, $match)) {
                $headers['version'] = $match['version'];
                $headers['status']  = (int)$match['status'];
            } else {
                list($key, $value) = explode(':', $lineContent, 2);
                $headers[$key] = ltrim($value);
            }
        }

        return new Message($headers, $body);
    }

    /**
     * Get formatted request headers
     *
     * @param array $headers
     * @return string
     */
    protected function getRequestHeaders(array $headers)
    {
        $requestHeaders = '';

        foreach ($headers as $name => $value) {
            $requestHeaders .= "$name: $value\r\n";
        }

        return $requestHeaders;
    }
}
