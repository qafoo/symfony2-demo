<?php

namespace Demo\BlogBundle\Service\SpamChecker\Akismet\HttpClient;

/**
 * HTTPClient connection exception
 */
class ConnectionException extends \RuntimeException
{
    public function __construct($url, $method)
    {
        parent::__construct("Could not connect to server $url.");
    }
}
