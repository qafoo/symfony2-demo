<?php

namespace Demo\BlogBundle\Service\SpamChecker;

use Demo\BlogBundle\Entity\Comment;

/**
 * @group integration
 */
class AkismetIntegrationTest extends \PHPUnit_Framework_TestCase
{
    protected function getApiKey()
    {
        // @TODO: Read API key from parameters.yml
        return null;
    }

    protected function getSpamChecker()
    {
        return new Akismet(
            new Akismet\HttpClient\Stream(),
            'http://example.com',
            $this->getApiKey()
        );
    }

    public function setUp()
    {
        if (!$this->getApiKey()) {
            $this->markTestSkipped("Missing API key.");
        }

        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['HTTP_REFERER'] = '';
    }

    public function testSpam()
    {
        $comment = new Comment();
        $comment->setText('Hello World');
        $comment->setAuthor('viagra-test-123');

        $spamChecker = $this->getSpamChecker();
        $this->assertTrue(
            $spamChecker->isSpam($comment)
        );
    }

    public function testNoSpam()
    {
        $comment = new Comment();
        $comment->setText('Hello World');
        $comment->setAuthor('Somebody');

        $spamChecker = $this->getSpamChecker();
        $this->assertFalse(
            $spamChecker->isSpam($comment)
        );
    }
}
