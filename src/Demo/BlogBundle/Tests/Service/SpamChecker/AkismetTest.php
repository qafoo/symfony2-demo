<?php

namespace Demo\BlogBundle\Service\SpamChecker;

use Demo\BlogBundle\Entity\Comment;

class AkismetTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['HTTP_REFERER'] = '';
    }

    public function testSpamCheckUrlBuilding()
    {
        $spamChecker = new Akismet(
            $httpClient = \Phake::mock('\\Demo\\BlogBundle\\Service\\SpamChecker\\Akismet\\HttpClient'),
            'http://example.com',
            'key'
        );

        \Phake::when($httpClient)
            ->request(\Phake::anyParameters())
            ->thenReturn(new Akismet\Message(array(), "true"));

        $comment = new Comment();
        $comment->setText('text');
        $comment->setAuthor('author');

        $spamChecker->isSpam($comment);

        \Phake::verify($httpClient)
            ->request(
                'POST',
                'http://key.rest.akismet.com/1.1/comment-check',
                new Akismet\Message(
                    array(),
                    'blog=http%3A%2F%2Fexample.com&user_ip=127.0.0.1&user_agent=DemoBlog' .
                    '&referrer=&comment_type=comment&comment_content=text&comment_author=author'
                )
            );
    }

    public function testSpam()
    {
        $spamChecker = new Akismet(
            $httpClient = \Phake::mock('\\Demo\\BlogBundle\\Service\\SpamChecker\\Akismet\\HttpClient'),
            'http://example.com',
            'key'
        );

        \Phake::when($httpClient)
            ->request(\Phake::anyParameters())
            ->thenReturn(new Akismet\Message(array(), "true"));

        $comment = new Comment();
        $comment->setText('text');
        $comment->setAuthor('author');

        $this->assertTrue(
            $spamChecker->isSpam($comment)
        );
    }

    public function testNoSpam()
    {
        $spamChecker = new Akismet(
            $httpClient = \Phake::mock('\\Demo\\BlogBundle\\Service\\SpamChecker\\Akismet\\HttpClient'),
            'http://example.com',
            'key'
        );

        \Phake::when($httpClient)
            ->request(\Phake::anyParameters())
            ->thenReturn(new Akismet\Message(array(), "false"));

        $comment = new Comment();
        $comment->setText('text');
        $comment->setAuthor('author');

        $this->assertFalse(
            $spamChecker->isSpam($comment)
        );
    }

    /**
     * @expectedException RuntimeException
     */
    public function testInvalidRequest()
    {
        $spamChecker = new Akismet(
            $httpClient = \Phake::mock('\\Demo\\BlogBundle\\Service\\SpamChecker\\Akismet\\HttpClient'),
            'http://example.com',
            'key'
        );

        \Phake::when($httpClient)
            ->request(\Phake::anyParameters())
            ->thenReturn(new Akismet\Message(array(), "invalid"));

        $comment = new Comment();
        $comment->setText('text');
        $comment->setAuthor('author');

        $spamChecker->isSpam($comment);
    }
}
