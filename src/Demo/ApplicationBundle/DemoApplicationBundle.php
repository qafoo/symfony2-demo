<?php

namespace Demo\ApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DemoApplicationBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
