<?php

namespace Demo\ApplicationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function userStateAction(Request $request)
    {
        return $this->render(
            'DemoApplicationBundle:User:userState.html.twig',
            array(
                'user' => $this->get('security.context')->getToken()->getUser(),
            )
        );
    }
}
