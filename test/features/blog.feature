Feature: Managing blog posts

    Scenario: Create a new blog post
        Given I am logged in
          And I am on "/post/"
         When I follow "Create a new entry"
          And I fill in the following:
                | demo_blogbundle_posttype_title  | New Blog Post |
                | demo_blogbundle_posttype_text   | Some text…    |
          And I press "Create"
         Then I should see "New Blog Post"

    Scenario: Anonymous may not create blog post
        Given I go to "/post/new"
         Then I should be on "/login"

    Scenario: Anonymous may see blog post
        Given I go to "/post/"
         Then I should see "New Blog Post"
