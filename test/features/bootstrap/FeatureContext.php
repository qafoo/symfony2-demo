<?php

use Behat\MinkExtension\Context\MinkContext,
    Behat\Behat\Exception\PendingException;

class FeatureContext extends MinkContext
{
    /**
     * @BeforeScenario
     */
    public function resetMinkSession()
    {
        // @HACK to remove all Symfony test sessions between tests.
        // Ensures every Scenario gets a clean state.
        array_map(
            'unlink',
            glob(__DIR__ . '/../../../app/cache/test/sessions/*')
        );
    }

    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $this->useContext('blog', new BlogContext($parameters));
    }
}
