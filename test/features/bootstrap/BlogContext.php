<?php

use Behat\Behat\Context\BehatContext,
    Behat\MinkExtension\Context\RawMinkContext,
    Behat\Behat\Exception\PendingException,
    Behat\Behat\Context\Step;

require 'PHPUnit/Autoload.php';

class BlogContext extends RawMinkContext
{
    protected static $registered = false;

    /**
     * @Given /^I am logged in$/
     */
    public function iAmLoggedIn()
    {
        if (self::$registered) {
            return array(
                new Step\Given('I am on "/login"'),
                new Step\When('I fill in "username" with "test"'),
                new Step\When('I fill in "password" with "password"'),
                new Step\When('I press "Login"'),
            );
        } else {
            self::$registered = true;
            return array(
                new Step\Given('I am on "/register"'),
                new Step\When('I fill in "fos_user_registration_form_username" with "test"'),
                new Step\When('I fill in "fos_user_registration_form_email" with "test@example.com"'),
                new Step\When('I fill in "fos_user_registration_form_plainPassword_first" with "password"'),
                new Step\When('I fill in "fos_user_registration_form_plainPassword_second" with "password"'),
                new Step\When('I press "Register"'),
            );
        }
    }
}
