Feature: User registration and login

    Scenario: Register User
        Given I am on the homepage
         When I follow "Register"
          And I fill in the following:
                | fos_user_registration_form_username             | testuser             |
                | fos_user_registration_form_email                | testuser@example.com |
                | fos_user_registration_form_plainPassword_first  | password             |
                | fos_user_registration_form_plainPassword_second | password             |
          And I press "Register"
         Then I should see "Congrats testuser, your account is now activated."

    Scenario: Fail on double registration
        Given I am on the homepage
         When I follow "Register"
          And I fill in the following:
                | fos_user_registration_form_username             | testuser             |
                | fos_user_registration_form_email                | testuser@example.com |
                | fos_user_registration_form_plainPassword_first  | password             |
                | fos_user_registration_form_plainPassword_second | password             |
          And I press "Register"
         Then I should see "The username is already used"

    Scenario: User Login
        Given I am on the homepage
         When I follow "Login"
          And I fill in the following:
                | username  | testuser  |
                | password  | password  |
          And I press "Login"
         Then I should see "Logout"
